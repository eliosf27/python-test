import datetime

from django import forms
from django.contrib import admin

from app_test import models


class VideoForm(forms.ModelForm):
    class Meta:
        model = models.Video
        fields = '__all__'

    def clean(self):
        date_uploaded = self.cleaned_data.get('date_uploaded')
        today = datetime.datetime.today().date()
        print(date_uploaded, today, (date_uploaded - today).days)
        if abs(date_uploaded - today).days >= 365:
            raise forms.ValidationError(
                "Please add videos with less than one year old."
            )

        return self.cleaned_data


@admin.register(models.Video)
class VideoAdmin(admin.ModelAdmin):
    form = VideoForm


@admin.register(models.Comment)
class CommentAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Theme)
class ThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Thumb)
class ThumbAdmin(admin.ModelAdmin):
    pass

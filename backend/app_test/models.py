# -*- coding: utf-8 -*-
import datetime

from django.db import models

from .operations import score


class Theme(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return '{}'.format(self.name)

    @property
    def score(self):
        final_score = 0
        for video in self.video_set.all():
            positive_comments = video.comment_set.filter(
                is_positive=True
            ).count()
            negative_comments = video.comment_set.filter(
                is_positive=False
            ).count()
            thumbs_up = video.thumb_set.filter(
                is_positive=True
            ).count()
            thumbs_down = video.thumb_set.filter(
                is_positive=False
            ).count()

            final_score += score(
                video.views, video.days_since_was_uploaded,
                positive_comments, negative_comments,
                thumbs_up, thumbs_down
            )
        return final_score


class Video(models.Model):
    title = models.CharField(max_length=100)
    date_uploaded = models.DateField()
    views = models.IntegerField(default=0)
    themes = models.ManyToManyField('Theme')

    def __str__(self):
        return '{}'.format(self.title)

    @property
    def days_since_was_uploaded(self):
        today = datetime.datetime.today().date()
        return (self.date_uploaded - today).days


class Comment(models.Model):
    time = models.DateField(auto_now_add=True)
    is_positive = models.BooleanField(
        default=False, verbose_name='Is positive?'
    )
    video = models.ForeignKey('Video')

    def __str__(self):
        return '{}: Is positive? {}'.format(self.video.title, self.is_positive)


class Thumb(models.Model):
    time = models.DateField(auto_now_add=True)
    is_positive = models.BooleanField(
        default=False, verbose_name='Is positive?'
    )
    video = models.ForeignKey('Video')

    def __str__(self):
        return '{}: Is positive? {}'.format(self.video.title, self.is_positive)

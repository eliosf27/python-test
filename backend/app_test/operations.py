# -*- coding: utf-8 -*-


def time_factor(days_since_upload):
    days = days_since_upload / 365
    return max(0, 1 - days)


def positive_numbers(positive, negative):
    if (positive + negative) > 0:
        return positive / (positive + negative)
    return 0


def positive_factor(*args):
    positive_comments, negative_comments, thumbs_up, thumbs_down = args
    good_comments = positive_numbers(positive_comments, negative_comments)
    thumbs_up = positive_numbers(thumbs_up, thumbs_down)
    return 0.7 * good_comments + 0.3 * thumbs_up


def score(views, days_since_was_upload, *args):
    positive_comments, negative_comments, thumbs_up, thumbs_down = args
    factor = positive_factor(
        positive_comments, negative_comments, thumbs_up, thumbs_down
    )
    return views * time_factor(days_since_was_upload) * factor
